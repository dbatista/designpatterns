package com.davidbatista.Strategy;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Sort Array Numbers.
 *
 * @author David
 * @since 14/04/2018
 */
public class SortArrayNumbers {
  public List<Integer> sort(List<Integer> list, OrderNumbersStrategy orderStrategy) {
    return orderStrategy.order(list.stream().collect(Collectors.toList()));
  }
}

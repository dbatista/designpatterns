package com.davidbatista.Strategy.SortingAlgorithm;

import java.util.List;

/**
 * Order Numbers Strategy.
 *
 * @author David
 * @since 14/04/2018
 */
public interface OrderNumbersStrategy {
  List<Integer> order(List<Integer> numbers);
}

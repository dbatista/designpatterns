package com.davidbatista.Strategy.SortingAlgorithm.implementation;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Description.
 *
 * @author David
 * @since 14/04/2018
 */
public class StreamOrderAscendant implements OrderNumbersStrategy {
  @Override
  public List<Integer> order(List<Integer> numbers) {
    return numbers.stream().sorted().collect(Collectors.toList());
  }
}

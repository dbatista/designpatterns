package com.davidbatista.Strategy.SortingAlgorithm.implementation;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.List;

/**
 * Description.
 *
 * @author David
 * @since 14/04/2018
 */
public class BubbleOrder implements OrderNumbersStrategy {
  public List<Integer> order(List<Integer> numbers) {
    for (int i = 1; i < numbers.size(); i++){
      for (int j = 0; j < numbers.size() - 1; j++){
        if (numbers.get(j) > numbers.get(j + 1)){
          int temporary = numbers.get(j);
          numbers.set(j, numbers.get(j + 1));
          numbers.set(j + 1, temporary);
        }
      }
    }
    return numbers;
  }
}

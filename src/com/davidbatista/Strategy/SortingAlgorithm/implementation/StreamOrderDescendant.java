package com.davidbatista.Strategy.SortingAlgorithm.implementation;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order Numbers Descendant.
 *
 * @author David
 * @since 14/04/2018
 */
public class StreamOrderDescendant implements OrderNumbersStrategy {
  public List<Integer> order(List<Integer> numbers) {
    return numbers.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
  }
}

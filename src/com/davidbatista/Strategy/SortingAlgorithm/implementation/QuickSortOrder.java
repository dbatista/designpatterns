package com.davidbatista.Strategy.SortingAlgorithm.implementation;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.List;

/**
 * Quick Sort Order.
 *
 * @author David
 * @since 14/04/2018
 */
public class QuickSortOrder implements OrderNumbersStrategy{
  public List<Integer> order(List<Integer> numbers) {
    quickSort(numbers, 0, numbers.size() - 1);
    return numbers;
  }

  private void quickSort(List<Integer> numbers, int izquierda, int derecha) {
    int pivote = numbers.get(izquierda);
    int i = izquierda;
    int j = derecha;
    int auxIntercambio;
    while (i < j) {
      while (numbers.get(i) <= pivote && i < j) {
        i++;
      }
      while (numbers.get(j) > pivote) {
        j--;
      }
      if (i < j) {
        auxIntercambio = numbers.get(i);
        numbers.set(i, numbers.get(j));
        numbers.set(j, auxIntercambio);
      }
    }
    numbers.set(izquierda,numbers.get(j));
    numbers.set(j, pivote);
    if (izquierda < j - 1) {
      quickSort(numbers, izquierda, j - 1);
    }
    if (j + 1 < derecha) {
      quickSort(numbers, j + 1, derecha);
    }
  }
}

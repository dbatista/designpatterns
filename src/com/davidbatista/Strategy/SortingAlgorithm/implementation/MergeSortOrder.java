package com.davidbatista.Strategy.SortingAlgorithm.implementation;

import com.davidbatista.Strategy.SortingAlgorithm.OrderNumbersStrategy;
import java.util.List;

/**
 * Description.
 *
 * @author David
 * @since 14/04/2018
 */
public class MergeSortOrder implements OrderNumbersStrategy {
  public List<Integer> order(List<Integer> numbers) {
    mergesort(numbers, 0, numbers.size());
    return numbers;
  }

  private void mergesort(List<Integer> numbers, int init, int n)
  {
    int n1;
    int n2;
    if (n > 1)
    {
      n1 = n / 2;
      n2 = n - n1;
      mergesort(numbers, init, n1);
      mergesort(numbers, init + n1, n2);
      merge(numbers, init, n1, n2);
    }
  }

  private void merge(List<Integer> numbers, int init, int n1, int n2)
  {
    int[] buffer = new int[n1+n2];
    int temp  = 0;
    int temp1 = 0;
    int temp2 = 0;
    int i;
    while ((temp1 < n1) && (temp2 < n2))
    {
      if (numbers.get(init + temp1) < numbers.get(init + n1 + temp2))
        buffer[temp++] = numbers.get(init + (temp1++));
      else
        buffer[temp++] = numbers.get(init + n1 + (temp2++));
    }
    while (temp1 < n1)
      buffer[temp++] = numbers.get(init + (temp1++));
    while (temp2 < n2)
      buffer[temp++] = numbers.get(init + n1 + (temp2++));
    for (i = 0; i < n1+n2; i++)
      numbers.set(init + i, buffer[i]);
  }
}

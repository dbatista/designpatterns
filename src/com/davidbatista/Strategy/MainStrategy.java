package com.davidbatista.Strategy;

import com.davidbatista.Strategy.SortingAlgorithm.implementation.BubbleOrder;
import com.davidbatista.Strategy.SortingAlgorithm.implementation.MergeSortOrder;
import com.davidbatista.Strategy.SortingAlgorithm.implementation.QuickSortOrder;
import com.davidbatista.Strategy.SortingAlgorithm.implementation.StreamOrderAscendant;
import com.davidbatista.Strategy.SortingAlgorithm.implementation.StreamOrderDescendant;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Main Strategy.
 *
 * @author David
 * @since 14/04/2018
 */
public class MainStrategy {
  public static void main(String[] args) {
    SortArrayNumbers sortArrayNumbers = new SortArrayNumbers();
    List<Integer> numbers = getInitialNumbers();

    System.out.println("List: " + numbers.toString());

    System.out.println("List Stream Asc: " +
        sortArrayNumbers.sort(numbers, new StreamOrderAscendant()).toString());

    System.out.println("List Stream Desc: " +
        sortArrayNumbers.sort(numbers, new StreamOrderDescendant()).toString());

    System.out.println("List Bubble: " +
        sortArrayNumbers.sort(numbers, new BubbleOrder()).toString());

    System.out.println("List MergeSort: " +
        sortArrayNumbers.sort(numbers, new MergeSortOrder()).toString());

    System.out.println("List QuickSort: " +
        sortArrayNumbers.sort(numbers, new QuickSortOrder()).toString());

    System.out.println("First List: " + numbers.toString());
  }

  private static List<Integer> getInitialNumbers() {
    Random random = new Random();
    Integer maxNumber = 500;
    return Arrays.asList(
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber),
        random.nextInt(maxNumber)
    );
  }
}
